package coremodule.loops;

public class WhileLoops {

    public static void main(String[] args) {

        // First Initialization , while condition , print statement then increment or decrement,
        int x =0;
         while ( x <10){
             System.out.println("while loop running "+x);

             if (x>6){
                 break;
             }
             x++;
         }
    }
}
