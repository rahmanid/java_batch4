package coremodule.datastructuresprac;

import java.util.HashMap;
import java.util.Hashtable;

public class HashTablePrac {

    public static void main(String[] args) {

        Hashtable<String, Object> hashtable = new Hashtable<>();

        hashtable.put("Name", "Rahman Amin Bhai");
        hashtable.put("salary", 120000);
        hashtable.put("age", 47);
        hashtable.put("position", "manager");

        System.out.println(hashtable);

        // hashtable.put("null","hgklsv");
        // hashtable threat safe

        // hashtable does not take null key or null value

        HashMap<String, Object> hmap = new HashMap<>();


        //HashMap is  a class implements map interface
        //extends AbstractMap
        //it contain only unique elements
        //store the values  as( key and value pair)
       // can take  one null key with multiple null value
        //it maintains no order
        //HashMap is non-Synchronized -- not thread safe
        hmap.put("chowdury", "chowdhury");
        hmap.put(null, "Talukder");
        hmap.put("SDD", "Talud");
        hmap.put(null, "sayeed");

        System.out.println(hmap);


    }
}
