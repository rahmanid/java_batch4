package coremodule.datastructuresprac;

import static coremodule.datastructures.Arrays.printMyArrays3;

public class Arrays {
    static String[] names = {"Rahman", "Ahmed", "khan", "chy", "talukdar"};

    public static void main(String[] args) throws Exception {

        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
        for (int j = names.length; j > 0; j--) {
            System.out.println(names[j - 1]);
        }

      //Try and catch  methods
        PrintMyArrays();
        printMyArrays2();
        printMyArrays3();
    }

    //catch and not fail
    public static void PrintMyArrays () {
        try {
            System.out.println(names[6]);
        } catch (ArrayIndexOutOfBoundsException ee) {
            System.out.println("failed");
        }
    }

     //throw  & fail
    public static void printMyArrays2 () throws Exception {
        System.out.println(names[6]);
    }

    //catch & still fail
    public static void printMyArrays3 () {
        try {
            System.out.println(names[6]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("exception in the loop, correct the loop");
            ex.printStackTrace();
        }
    }
}