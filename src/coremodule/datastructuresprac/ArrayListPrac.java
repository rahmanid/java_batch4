package coremodule.datastructuresprac;

import java.util.ArrayList;

public class ArrayListPrac {
    public static void main(String[] args) {

        ArrayList arrayList = new ArrayList();
        arrayList.add("ahnaf");
        arrayList.add("rahman");
        arrayList.add("abid");
        arrayList.add("akif");
        arrayList.add("inayet");

        System.out.println(arrayList.size());

        System.out.println(arrayList.get(4));

        System.out.println("******--------*******");

        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        System.out.println("******---------*********");

        for (int j = arrayList.size(); j > 0; j--) {
            System.out.println(arrayList.get(j - 1));
        }
    }
}
