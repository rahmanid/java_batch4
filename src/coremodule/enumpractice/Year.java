package coremodule.enumpractice;

public class Year {
    Months months;

   /* public Year(Months months){
        this.months = months;
    }*/

    public void weatherOfTheYear(Months months){

        switch (months){
            case January:
                System.out.println("cooled");
                break;
            case February:
            case March:
            case May:
                System.out.println("its getting better");
                break;
            case July:
            case June:
                System.out.println("its summer");
                break;
            default:
                System.out.println("not interested");
                break;

        }
    }

    public static void main(String[] args) {
        Year year = new Year();
        year.weatherOfTheYear(Months.December);
    }
}
