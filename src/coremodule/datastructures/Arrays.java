package coremodule.datastructures;

public class Arrays {

    static String[] names = {"opu", "sowrov", "adnan", "Ahnaf"};


    public static void main(String[] args) throws Exception {
        String name = "Ahnaf Rahman ";
        System.out.println(name);
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

//        try {
//            System.out.println(names[4]);
//        } catch (ArrayIndexOutOfBoundsException ex) {
//            System.out.println("exception in the loop,please correct the loop");
//            ex.printStackTrace();
//        }

        // catch & not fail
        printMyArrays();
        //throw & fail
        printMyArrays2();
        //catch  & still fail
        printMyArrays3();




    }

    public static void printMyArrays3() {
        try {
            System.out.println(names[6]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("exception in the loop,please correct the loop");
            ex.printStackTrace();

        }


    }

    public static void printMyArrays2() throws Exception {
        System.out.println(names[5]);

    }

    public static void printMyArrays() {
        try {
            System.out.println(names[5]);
        } catch (ArrayIndexOutOfBoundsException ee) {
            System.out.println("failed");
        }

    }


}