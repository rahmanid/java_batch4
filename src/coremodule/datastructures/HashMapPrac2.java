package coremodule.datastructures;

import coremodule.oop.inheritance.A;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapPrac2 {

    public static void main(String[] args) {

        ArrayList<String> citysArrayList = new ArrayList<>();
        citysArrayList.add("rego park");
        citysArrayList.add("ozone park");
        citysArrayList.add("prospect park");

        ArrayList<String> zipcodeAsArrayList = new ArrayList<>();

        zipcodeAsArrayList.add("11347");
        zipcodeAsArrayList.add("11417");
        zipcodeAsArrayList.add("11208");

        HashMap<String, ArrayList<String>> stateMap = new HashMap<>();

        stateMap.put("city", citysArrayList);
        stateMap.put("zipcode", zipcodeAsArrayList);

        System.out.println(stateMap);

        ArrayList<String> kyes = new ArrayList<>(stateMap.keySet());

        for (int i = 0; i < stateMap.size(); i++) {
            System.out.println(stateMap.get(kyes.get(i)));
        }

        System.out.println("****************");

        for (int i = 0; i < stateMap.size(); i++) {
            for (int j = 0; j < stateMap.get(kyes.get(i)).size(); j++)
                System.out.println(stateMap.get(kyes.get(i)).get(j));
        }

    }
}
