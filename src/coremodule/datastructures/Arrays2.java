package coremodule.datastructures;

public class Arrays2 {
    public static void main(String[] args) {
        String[] names = new String[4];

        names[0] = "opu";
        names[1] = "adnan";

        System.out.println(names[1]);
        System.out.println(names[3]);

        System.out.println(names.length);
    }
}
