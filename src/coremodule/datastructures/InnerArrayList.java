package coremodule.datastructures;

import java.util.ArrayList;

public class InnerArrayList {

    public static void main(String[] args) {


        ArrayList<String> city = new ArrayList<>();

        city.add("rego park");
        city.add("ozone park");
        city.add("Prospect park");


        ArrayList<String> zipcode = new ArrayList<>();
        zipcode.add("11414");
        zipcode.add("11208");
        zipcode.add("11417");

        ArrayList<String> boro=new ArrayList<>();
        boro.add("Queens");
        boro.add("Brooklyn");
        boro.add("Manhattan");

        ArrayList<ArrayList<String>> states = new ArrayList<>();
        states.add(city);
        states.add(zipcode);
        states.add(boro);
        System.out.println(states);
        System.out.println(states.get(0));
        System.out.println(states.get(0).get(1));
        System.out.println(states.get(1));
        System.out.println(states.get(1).get(2));

          //State size 3
        for (int i = 0; i < states.size(); i++) {
            for (int j = 0; j < states.get(i).size(); j++) {
                System.out.println(states.get(i).get(j));

            }
        }


    }
}
