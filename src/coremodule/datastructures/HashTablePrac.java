package coremodule.datastructures;

import java.util.HashMap;
import java.util.Hashtable;

public class HashTablePrac {
    public static void main(String[] args) {

        Hashtable<String,Object> hashtable = new Hashtable<>();

        hashtable.put("name","Aminur");
        hashtable.put("age",40);
        hashtable.put("salary",120000);

       // hashtable.put("null","chcjhvc");
        // hashtable threat safe

        // hashtable does not take null key or null value

        System.out.println(hashtable);

        HashMap<String,Object> map = new HashMap<>();

        map.put(null,"Ahnaf");
        map.put("ghs",null);
        map.put("rahman",null);
        map.put("Sadik",null);

        System.out.println(map);
    }
}
