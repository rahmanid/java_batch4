package coremodule.datastructures;

import java.util.ArrayList;

public class ArrayListPrac {
    public static void main(String[] args) {

        ArrayList arrayList = new ArrayList();
        arrayList.add("opu");
        arrayList.add("adnan");
        arrayList.add("ahnaf");

        System.out.println(arrayList.size());

        System.out.println(arrayList.get(0));

        for(int i =0;i <arrayList.size();i++){
            System.out.println(arrayList.get(i));
        }

        System.out.println("************");
        for(int i = arrayList.size();i>0;i--){
            System.out.println(arrayList.get(i-1));
        }


    }
}
