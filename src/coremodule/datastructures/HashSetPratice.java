package coremodule.datastructures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class HashSetPratice {

    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();

        set.add("sowrov");
        set.add("Ahnaf");
        set.add("khan");


        System.out.println(set);
// hash set always print unique value
        ArrayList<String> list = new ArrayList<>(set);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("************");

        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }
}
