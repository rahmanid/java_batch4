package coremodule.datastructures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HashMapPrac {
    public static void main(String[] args) {

        HashMap<String,String> states = new HashMap<>();
        states.put("rego park","11374");
        states.put("ozone park","11417");
        states.put("brooklyn","11208");

       System.out.println(states.get("rego park"));

        System.out.println(states.size());

        ArrayList<String> keysAsArray = new ArrayList<>(states.keySet());
        System.out.println(keysAsArray);

        for(int i =0;i<states.size();i++){
            System.out.println(states.get(keysAsArray.get(i)));
        }
        System.out.println("_______________________________");

        //getting all the values from HashMap using for()

        //Entry is an interface which is used to traverse HashMap
        for (Map.Entry m:states.entrySet()){
            System.out.println(m.getKey()+" "+ m.getValue());
        }

        //for removing

       /*  states.remove(1);
       System.out.println(states);

        */
    }
}
