package coremodule.enumpractice2;

public class Salary {
    Positions positions;

    public void SalaryOfTheWeek(Positions positions){
        switch (positions){
            case FloorMan:
            case Cashier:
                System.out.println("weekly salary $700 dolors");
                break;
            case MeatCutter:
                System.out.println("weekly salray $800 dolors");
                break;
            case AssistantManager:
                System.out.println("weekly salary $ 1000 dolors");
                break;
            default:
                System.out.println("weekly salary $1300 dolors");
                break;

        }
    }

    public static void main(String[] args) {
        Salary salary = new Salary();
        salary.SalaryOfTheWeek(Positions.Cashier);
        salary.SalaryOfTheWeek(Positions.AssistantManager);
        salary.SalaryOfTheWeek(Positions.Supervisor);

    }
}
