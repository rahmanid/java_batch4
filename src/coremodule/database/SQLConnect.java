package coremodule.database;

import java.sql.*;

public class SQLConnect {

    public static void main(String[] args) throws SQLException {
        String username = "root";
        String password = "root1234";
        String databaseName = "bstch4";
        //jdbc:mysql - driver name
        //localhost - server name
        //3306 - server port
        String url = "jdbc:mysql://localhost:3306/" + databaseName;

        //?serverTimezone  = UTC--- if server timezone error comes

        String query = "select * from student;";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        while (table.next()) {
            int id = table.getInt("id");
            String name = table.getString("name");
            String location = table.getString("location");
            System.out.println(id + " " + name + " " + location);

        }

    }
}
