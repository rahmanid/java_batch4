package coremodule.database;

import java.sql.*;
import java.util.ArrayList;

public class SQLConnect_4 {
    public static Connection getConnection(String username, String password, String databaseName) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/" ;//
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"+databaseName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(url, username, password);
        return connection;
    }


    public static void main(String[] args) throws SQLException {
        Connection connection = getConnection("root", "root1234", "bstch4");
        String query = "select * from student;";

        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        //3 arr
        ArrayList<String> names = new ArrayList<>();
        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> location = new ArrayList<>();
        while (table.next()) {
            id.add(table.getInt("id"));
            names.add(table.getString("name"));
            location.add(table.getString("location"));
            System.out.println(id);
            System.out.println(names);
            System.out.println(location);

            ResultSetMetaData resultSetMetaData = table.getMetaData();
            int number = resultSetMetaData.getColumnCount();
            System.out.println(number);

            // statement.close();
            //  connection.close();
        }
    }

    public static void cleanUpDatabase(Statement statement, Connection connection) {
        try {
            statement.close();
            connection.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }


}