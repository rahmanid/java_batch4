package coremodule.database.database_Prac;

import java.sql.*;
import java.util.ArrayList;

public class DbConnect4 {

    public static Connection getConnection(String username, String password,String dbname) throws SQLException {

        String url = "jdbc:mysql://localhost:3306/" + dbname;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

    public static void main(String[] args) throws SQLException {
        Connection connection = getConnection("root","root1234","bstch4");
        String query = "select * from student;";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> location = new ArrayList<>();

        while (resultSet.next()) {
            id.add(resultSet.getInt("id"));
            name.add(resultSet.getString("name"));
            location.add(resultSet.getString("location"));
            System.out.println(id);
            System.out.println(name);
            System.out.println(location);

            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int num = resultSetMetaData.getColumnCount();
            System.out.println(num);
        }

        cleanUpDatabase(statement, connection);
    }

    private static void cleanUpDatabase(Statement statement, Connection connection) {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}