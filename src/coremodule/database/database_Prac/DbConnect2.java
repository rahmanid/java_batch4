package coremodule.database.database_Prac;


import java.sql.*;
import java.util.ArrayList;

public class DbConnect2 {

    public static void main(String[] args) throws SQLException {

        String userName = "root";
        String passWord = "root1234";
        String dbname = "bstch4";

        String url = "jdbc:mysql://localhost:3306/" + dbname;
        String query = "select * from student;";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url, userName, passWord);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> location = new ArrayList<>();


        while (resultSet.next()) {
            id.add(resultSet.getInt("id"));
            names.add(resultSet.getString("name"));
            location.add(resultSet.getString("location"));
        }
            System.out.println(id);
            System.out.println(names);
            System.out.println(location);

            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
           int num = resultSetMetaData.getColumnCount();
            System.out.println(num);



    }


}
