package coremodule.database.database_Prac;

import java.sql.*;
import java.util.ArrayList;

public class SQLConnectPrac2 {

    public static void main(String[] args) throws SQLException {
        String username = "root";
        String password = "root1234";
        String dbname = "bstch4";

        String url = "jdbc:mysql://localhost:3306/"+dbname;
        String query = "select * from student;";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url,username,password);
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> location = new ArrayList<>();

        while (table.next()){
            id  .add( table.getInt("id"));
            name.add(table.getString("name"));
            location.add(table.getString("location"));
        }
        System.out.println(id);
        System.out.println(name);
        System.out.println(location);

        ResultSetMetaData resultSetMetaData = table.getMetaData();
       int number = resultSetMetaData.getColumnCount();
        System.out.println(number);


    }
}
