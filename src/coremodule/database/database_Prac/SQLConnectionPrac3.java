package coremodule.database.database_Prac;

import java.sql.*;

public class SQLConnectionPrac3 {
    //global or class label variable
    static  String query= "select * from student;" ;

    //method for connection
    public static Connection getConnection() throws SQLException {
        String username ="root";
        String password = "root1234";
        String dbname = "bstch4";
        String url = "jdbc:mysql://localhost:3306/"+dbname;

        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // initializing connection (connection is a variable )
        Connection connection = DriverManager.getConnection(url,username,password);

        //returning the connection
        return connection;
    }
    public static void main(String[] args) throws SQLException {
        //stored the connection in (connection variable)
        Connection connection = getConnection();
        // Statements allow to issue SQL queries to the database
        Statement statement = connection.createStatement();
        // Result set get the result of the SQL query
        ResultSet table = statement.executeQuery(query);

        while (table.next()){
            int id = table.getInt("id");
            System.out.println(id);
        }
        dataBaseCleaup(statement,connection);
    }

    static void  dataBaseCleaup(Statement statement, Connection connection){
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
