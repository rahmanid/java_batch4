package coremodule.database.database_Prac;

import java.sql.*;

public class SQLConnectPrac {

    public static void main(String[] args) throws SQLException {
        String username = "root";
        String password = "root1234";
        String dbname = "bstch4";

        String url = "jdbc:mysql://localhost:3306/"+dbname;
        String query = "select * from student;";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = DriverManager.getConnection(url,username,password);
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        while (table.next()){
           int id = table.getInt("id");
           String name= table.getString("name");
           String location = table.getString("location");
            System.out.println("id= "+id+" "+"name= "+name+" "+"location= "+location);
        }
    }
}
