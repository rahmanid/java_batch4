package coremodule.database.database_Prac;

import java.sql.*;

public class SqlConnectionPrac_4 {
    public static Connection getCooncetion(String username, String password,String databaseName) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(url+databaseName , username, password);
        return connection;


    }

    public static void main(String[] args) throws SQLException {
        String query="select * from student;";
        Connection connection= getCooncetion("root","root1234","bstch4");
        Statement statement=connection.createStatement();
        ResultSet table=statement.executeQuery(query);


        while (table.next()){
            int id=table.getInt("id");
            String name = table.getString("name");
            String location = table.getString("location");
            System.out.println(id+" "+name+" "+location);
        }
        cleanUpMethod(statement,connection,table);


    }

    static void cleanUpMethod(Statement statement,Connection connection,ResultSet resultSet){
        try {
            statement.close();
            connection.close();
            resultSet.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
