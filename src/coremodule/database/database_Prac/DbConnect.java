package coremodule.database.database_Prac;

import java.sql.*;

public class DbConnect {
    public static void main(String[] args) throws SQLException {

        //Database login username,password and database name
        String userName = "root";
        String passWord = "root1234";
        String dbname = "bstch4";

        //Database url
        String url = "jdbc:mysql://localhost:3306/" + dbname;
        String query = "select * from student ;";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(url, userName, passWord);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);


        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String location = resultSet.getString("location");

            System.out.println("ID :- " + id + " " + "Name :- " + name + " " + "Location :-  " + location);
        }


    }


}
