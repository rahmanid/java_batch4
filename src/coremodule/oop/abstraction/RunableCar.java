package coremodule.oop.abstraction;

public class RunableCar extends Car implements Vehicle {
    @Override
    public void move() {
        System.out.println("car can move");

    }

    @Override
    public void start() {
        System.out.println("car can start");

    }

    @Override
    public void stop() {
        System.out.println("car can stop");

    }

    @Override
    public void name() {
        System.out.println("car name is BMW");

    }

    @Override
    public void wheels() {
        System.out.println("car has 4 wheels");

    }

    //Interface-->Implements (I-I)
    //class/Abstract class-->extent
    // extend first,then implements
    //implement multiple interface
    // can"t extend more then one class/abstract class


}
