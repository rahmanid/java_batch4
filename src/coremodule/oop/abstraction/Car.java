package coremodule.oop.abstraction;

public abstract class Car {

    public abstract void wheels();

    public void color(){
        System.out.println("red");

        // abstract class can have method with/without body
        // abs

    }

}
