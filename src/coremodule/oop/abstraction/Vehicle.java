package coremodule.oop.abstraction;

public interface Vehicle {

    // interface is just an concept
    //

    public void move();
    public void start();
    public void stop();
    public void name();
    // can't have method with body
    //can't have constructor
    //can have return/void type method names--> not body
}
