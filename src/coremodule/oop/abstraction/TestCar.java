package coremodule.oop.abstraction;

public class TestCar {
    public static void main(String[] args) {
        RunableCar runableCar =new RunableCar();
        runableCar.move();
        runableCar.name();
        runableCar.start();
        runableCar.stop();
        runableCar.wheels();
        runableCar.color();

        FutureCar futureCar = new FutureCar();
        futureCar.color();
    }

}
