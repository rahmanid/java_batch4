package coremodule.oop.inheritance;

public class B extends A {
    public static void main(String[] args) {
        B b= new B();
        b.methodFromA();
        b.methodFromB();
    }
    // method overridden


    public void methodFromA(){
        System.out.println("method details from B class");
    }
    public void methodFromB(){
        System.out.println("method details from B class");
    }
}
