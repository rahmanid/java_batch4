package coremodule.oop.polymorphism;

public class PolymorphismCalculator {

    //polymorphism--> having many forms

    public static void main(String[] args) {
        PolymorphismCalculator calculator = new PolymorphismCalculator();
        calculator.addition(10,20,25);


    }
    // method overloading --same method,different parameter,same class
    // known as static polymorphism/compile time polymorphism
    public void addition(int x,int y,int z){
        System.out.println(x+y+z);

        // method overriding --same method ,same parameter,different class
        // known as dynamic polymorphism/run time polymorphism
    }
}
