package coremodule.oop.abstructionbusiness;

public class TestFoodBusiness {

    public static void main(String[] args) {
        Grocery grocery = new Grocery();
        grocery.name();
        grocery.cooler();
        grocery.rac();
        grocery.knife();
        grocery.meat();
        grocery.fruit();
        grocery.vegetable();
        grocery.name();
        String g = grocery.name2();
        System.out.println(g);


        FutureFoodBusiness futureFoodBusiness = new FutureFoodBusiness();
        futureFoodBusiness.frezer();
        futureFoodBusiness.frozenfish();
        futureFoodBusiness.meatcuttermachine();
    }
}
