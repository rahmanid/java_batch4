package coremodule.oop.abstructionbusiness;

public interface Business {

    public void fruit();
    public void vegetable();
    public void meat();
    public String name2();


}
