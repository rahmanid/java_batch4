package coremodule.oop.abstructionbusiness;

public class Grocery extends FoodBusiness implements Business {
    @Override
    public void fruit() {
        System.out.println("Need all kind of fruit");
    }

    @Override
    public void vegetable() {
        System.out.println("need all kind of vegetable");

    }

    @Override
    public void meat() {
        System.out.println("need beef,goat and lamb meat");

    }

    @Override
    public String name2() {
        return "Supermarket 2";
    }


    @Override
    public void cooler() {
        System.out.println("need  two kinds of cooler");

    }

    @Override
    public void rac() {
        System.out.println("I need vagetable rac");

    }

    @Override
    public void knife() {
        System.out.println("I need meet cutting knife");

    }
}
