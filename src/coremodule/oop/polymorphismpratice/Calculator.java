package coremodule.oop.polymorphismpratice;

public class Calculator {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        calculator.addition(15,10);
        calculator.addition(15,"APU");
        calculator.addition(10,15,20);


    }
        public void addition(int x, int y){
            System.out.println(x+y);

        }
        public void addition(int x,String y){

            System.out.println(x+y);

        }
        public void addition(int x,int y,int z){
            System.out.println(x+y+z);

        }



}
