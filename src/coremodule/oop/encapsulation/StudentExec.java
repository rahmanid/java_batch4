package coremodule.oop.encapsulation;

import coremodule.oop.encapsulation.Student;

public class StudentExec {

    public static void main(String[] args) {
        Student student  = new Student();
        student.setId(25);
        int idOfStudent = student.getId();
        System.out.println ("id = "+ idOfStudent);

        student.setName("Ahnaf");
        String name = student.getName();
        System.out.println("name ="+name);


    }
}
