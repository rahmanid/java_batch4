package coremodule.oop.encapsulation;

public class Student {

    // encapsulation --- data hiding
    // use private global variable

    private int id;
    private String name;
    private String college;
    private String dob;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;


    }
}
