package coremodule.oop.encapsulationpractice;

public class EmployeeExec {

    public static void main(String[] args) {
        Employee employee = new Employee();

        employee.setName("Rahman");
        String nm = employee.getName();

        employee.setId("02A");
        String emid = employee.getId();

        employee.setSalary(15000);
        int emsa = employee.getSalary();

        employee.setBonus(2000);
        int embo = employee.getBonus();

        System.out.println("Employee name is : " + nm);
        System.out.println("Employee id is : " + emid);
        System.out.println("Employee salary is : " + emsa);
        System.out.println("Employee bonus is  : " + embo);



    }
}
